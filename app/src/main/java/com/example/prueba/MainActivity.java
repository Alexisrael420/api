package com.example.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtContrasenia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUsuario = (EditText) findViewById(R.id.txtUser);
        txtContrasenia = (EditText) findViewById(R.id.txtClave);

    }

    public void Iniciar(View v){
        String Usuario = txtUsuario.getText().toString();
        String Contrasenia = txtContrasenia.getText().toString();
        if(Usuario.equals("alex@gmail.com") && Contrasenia.equals("12345")){
            Intent abrir = new Intent(MainActivity.this, SegundaActivity.class);
            startActivity(abrir);
        }else {
            Toast.makeText(MainActivity.this, "USUARIO O CONTRASEÑA INCORRECTOS", Toast.LENGTH_LONG).show();
        }

    }
}